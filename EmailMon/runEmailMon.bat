@echo off

set JAVA=%JAVA_HOME%\bin\java
REM set JAVA=.\jre\bin\java

set EMAIL_MON_CLASSPATH=lib\EmailMonitor.jar;lib\mailapi.jar;lib\smtp.jar;lib\dsn.jar;lib\imap.jar;lib\pop3.jar;lib\appframework-1.0.3.jar;lib\swing-worker-1.1.jar


set VM_OPTIONS=-Xms128m -Xmx1024m

set MAIN_CLASS=emailmonitor.EmailMonitorApp

"%JAVA%" -classpath "%EMAIL_MON_CLASSPATH%" %VM_OPTIONS% %MAIN_CLASS%

