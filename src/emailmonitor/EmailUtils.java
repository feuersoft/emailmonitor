/*
 * Copyright (C) 2020, FeuerSoft.
 */
package emailmonitor;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Fritz Feuerbacher
 */
public class EmailUtils
{
  // These are default email parameters for connecting to the email server.
  private  final static int    EMAIL_POP3_PORT = 110;
  private  final static String MAIL_CLIENT_RETRIEVE_POP3 = "pop3";
  private  final static String EMAIL_SERVER = "localhost";
  private  final static String TRANSPORT_TYPE = "smtp";
  private  final static String mailFolderName = "INBOX";

  /*
   * Initializes and creates a connection to the email server.
   */
  public static Session initializeEmail(String host,
                                        Integer port,
                                        String fromAddr,
                                        String cfgFileName)
  {
    Session mailSession = null;
    Properties props = new Properties();

    props.put("mail.smtp.host", host);
    props.put("mail.smtp.port", port.toString());
    props.put("mail.smtp.from", fromAddr);

    List mailcfg = Utils.getMailConfig(cfgFileName);

    for (int i=0; i<mailcfg.size()-1; i+=2)
    {
       props.put((String)mailcfg.get(i), (String)mailcfg.get(i+1));
    }

    System.out.println(props.toString());
    mailSession = Session.getInstance(props, null);

    return mailSession;
  }

  /*
   * Creates an email message and will email it if the send parameter is true.
   */
  public static MimeMessage createEmailMessage(EmailConfiguration ecfg,
                                               String toAddr,
                                               String subject,
                                               String body,
                                               boolean send)
  {
    MimeMessage emailMessage = null;

    if (null != ecfg && null != ecfg.mailSession)
    {
       emailMessage = new MimeMessage(ecfg.mailSession);
       try
       {
           emailMessage.setFrom();
           emailMessage.setRecipient(Message.RecipientType.TO, 
                                     new InternetAddress(toAddr));
           emailMessage.setSubject(subject);
           emailMessage.setText(body);
           if (send)
           {
              if (ecfg.serverRequiresAuth)
              {
                 Transport tr = ecfg.mailSession.getTransport(TRANSPORT_TYPE);
                 tr.connect(ecfg.serverHost, ecfg.loginName, ecfg.loginPassword);
                 emailMessage.saveChanges();
                 tr.sendMessage(emailMessage, emailMessage.getAllRecipients());
                 tr.close();
              }
              else
              {
                 Transport.send(emailMessage);
              }
           }
       }
       catch (MessagingException e)
       {
         e.printStackTrace();
       }
    }

    return emailMessage;
  }
}
