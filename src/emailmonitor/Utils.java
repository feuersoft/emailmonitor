/*
 * Copyright (C) 2020, FeuerSoft.
 */
package emailmonitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * The <code>Utils</code> class provides common services.
 * All methods are static and should not store state between
 * calls.
 *
 * @author Fritz Feuerbacher
 * @since JDK1.6
 */
public class Utils
{
   private static final Properties buildDateProperty = new Properties();
   static
   {
      try
      {
         ClassLoader cld = Thread.currentThread().getContextClassLoader();
         InputStream bldProps = cld.getResourceAsStream("build.properties");
         buildDateProperty.load(bldProps);
         System.out.println(buildDateProperty.getProperty("BuildDate"));
      }
      catch (IOException ex)
      {
         ex.printStackTrace();
      }
   }

   private static final String NL = "\r\n";
   public  static final String COPYRIGHT = "Copyright © FeuerSoft 2020";
   public  static final String JRE_VERSION = "Java Version: " + System.getProperty("java.version");
   public  static final String BUILD_DATE = "Built: " + buildDateProperty.getProperty("buildDate");

   public static final SimpleDateFormat time24SlashFormat =
                          new SimpleDateFormat("MM'/'dd'/'yyyy HH:mm:ss");
  
   public static final SimpleDateFormat time24Format =
                          new SimpleDateFormat("MMM d, yyyy kk:mm:ss");
  
   public static final SimpleDateFormat time12Format =
                       new SimpleDateFormat("MMM d, yyyy hh:mm:ss a");
   
   public static final SimpleDateFormat time24FileNameFormat =
                          new SimpleDateFormat("MMMddyyyykkmmss");
   
   public static final SimpleDateFormat dtgDateFormat =
                                new SimpleDateFormat("ddkkmm'Z'MMMyyyy");
   
   public static final SimpleDateFormat dtgWeekday = 
                                new SimpleDateFormat("EEEE");
   
   public static final SimpleDateFormat dtgDay = 
                                new SimpleDateFormat("dd");
   
   public static final SimpleDateFormat dtgTime = 
                                new SimpleDateFormat("kkmm");
   
   public static final SimpleDateFormat dtgHour = 
                                new SimpleDateFormat("H");
   
   public static final SimpleDateFormat dtgNormalDate = 
                                new SimpleDateFormat("M'/'d'/'yyyy");   
      
   public static final SimpleDateFormat dtgMonthNum = 
                                new SimpleDateFormat("M");
   
   public static final SimpleDateFormat dtgMonth = 
                                new SimpleDateFormat("MMM");      
   
   public static final SimpleDateFormat dtgYear = 
                                new SimpleDateFormat("yyyy");

   public static final long MILLISECS_IN_SEC  = 1000L;
   public static final long MILLISECS_IN_MIN  = 60000L;
   public static final long MILLISECS_IN_HOUR = 3600000L;
   public static final long MILLISECS_IN_DAY  = 86400000L;
   public static final long MILLISECS_IN_3DAY = 259200000L;
   public static final long MILLISECS_IN_WEEK = 604800000L;

   public static final String COMMENT_DELIM = ":";
   public static final String INPUT_ASK = "Select an Option";

   public static final String MAIL_CFG_FILE_NAME = "mail_config.cfg";


   public static String getExtension(File f)
   {
     String ext = null;
     if (null != f)
     {
       String s = f.getName();
       int i = s.lastIndexOf('.');

       if (i>0 && i<s.length()-1)
       {
         ext = s.substring(i+1).toLowerCase();
       }
     }
     return ext; 
   }

   public static List<String> getMailConfig(String filename)
   {
      List<String> tokens = new ArrayList<>();

      File file = new File(filename);
      BufferedReader bufRdr;
      String line = null;
      try
      {
         bufRdr = new BufferedReader(new FileReader(file));
         while ((line = bufRdr.readLine()) != null)
         {
            if (!line.contains(COMMENT_DELIM))
            {
               StringTokenizer st = new StringTokenizer(line, "=");
               while (st.hasMoreTokens())
               {
                  tokens.add(st.nextToken());
               }
            }
         }
         bufRdr.close();
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
      return tokens;
   }
}
