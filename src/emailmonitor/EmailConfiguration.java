/*
 * Copyright (C) 2020, FeuerSoft.
 */
package emailmonitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Session;

/**
 * @author Fritz Feuerbacher
 */
public class EmailConfiguration
  implements Serializable
{
   private static final long serialVersionUID = 01L;

   public boolean serverRequiresAuth = false;
   public List<String> monitorFolders = new ArrayList<>();
   public List<String> toAddresses = new ArrayList<>();
   public String loginPassword = "admin";
   public String loginName = "admin";
   public Integer serverPort = 25;
   public String serverHost = "localhost";
   public String fromAddress = "emailmon@192.168.0.17";
   public Boolean combineReports = false;
   transient public Session mailSession = null;
}
