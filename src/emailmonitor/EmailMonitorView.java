/*
 * Copyright (C) 2020, FeuerSoft.
 */
package emailmonitor;

import java.io.IOException;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;

/**
 * @author Fritz Feuerbacher
 */
public class EmailMonitorView
  extends FrameView
{
   private final String CONFIG_FILENAME = "monitor_config.dat";
   private final int    STATUS_BAR_MAX  = 200;
   private EmailConfiguration emailCfg = new EmailConfiguration();
   private Timer updateTimer = new Timer(10000,
                                         new MonitorFoldersActionListener());
   private Integer amountProcessed = 0;
  
   public EmailMonitorView(SingleFrameApplication app)
   {
        super(app);
        initComponents();
        updateTimer.setCoalesce(true);

        InputStream is;
        try
        {
          is = new FileInputStream(CONFIG_FILENAME);
          ObjectInput oi;
          oi = new ObjectInputStream(is);
          emailCfg = (EmailConfiguration) oi.readObject();
          oi.close();
        }
        catch (IOException ex) {}
        catch (ClassNotFoundException ex2) {}

        jCheckBoxServerRequiresAuthentication.setSelected(emailCfg.serverRequiresAuth);
        jTextFieldEmailServerLoginName.setEnabled(jCheckBoxServerRequiresAuthentication.isSelected());
        jPasswordFieldMailServerLoginPassword.setEnabled(jCheckBoxServerRequiresAuthentication.isSelected());

        for (String str : emailCfg.monitorFolders)
        {
          ((DefaultListModel) jListMonitorFolders.getModel()).addElement(str);
        }
        for (String str : emailCfg.toAddresses)
        {
          ((DefaultListModel) jListToAddresses.getModel()).addElement(str);
        }

        jPasswordFieldMailServerLoginPassword.setText(emailCfg.loginPassword);
        jTextFieldEmailServerLoginName.setText(emailCfg.loginName);
        jSpinnerSmtpPort.setValue(emailCfg.serverPort);
        jTextFieldEmailServerHost.setText(emailCfg.serverHost);
        jTextFieldFromAddress.setText(emailCfg.fromAddress);

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });

        progressBar.setIndeterminate(false);
        progressBar.setMinimum(0);
        progressBar.setMaximum(200);
        getFrame().setIconImage(resourceMap.getImageIcon("Application.icon").getImage());
   }

   @Action
   public void showAboutBox() {
       if (aboutBox == null) {
           JFrame mainFrame = EmailMonitorApp.getApplication().getMainFrame();
           aboutBox = new EmailMonitorAboutBox(mainFrame);
           aboutBox.setLocationRelativeTo(mainFrame);
       }
       EmailMonitorApp.getApplication().show(aboutBox);
   }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      mainPanel = new javax.swing.JPanel();
      jPanel1 = new javax.swing.JPanel();
      jTextFieldFromAddress = new javax.swing.JTextField();
      jLabel1 = new javax.swing.JLabel();
      jScrollPane1 = new javax.swing.JScrollPane();
      jListToAddresses = new javax.swing.JList();
      jButtonAddEmailAddress = new javax.swing.JButton();
      jButtonRemoveEmailAddress = new javax.swing.JButton();
      jTextFieldEmailServerHost = new javax.swing.JTextField();
      jTextFieldEmailServerLoginName = new javax.swing.JTextField();
      jPasswordFieldMailServerLoginPassword = new javax.swing.JPasswordField();
      jCheckBoxServerRequiresAuthentication = new javax.swing.JCheckBox();
      jLabel2 = new javax.swing.JLabel();
      jSpinnerSmtpPort = new javax.swing.JSpinner();
      jLabel3 = new javax.swing.JLabel();
      jScrollPane2 = new javax.swing.JScrollPane();
      jListMonitorFolders = new javax.swing.JList();
      jButtonAddFolder = new javax.swing.JButton();
      jButtonRemoveFolder = new javax.swing.JButton();
      jCheckBoxDeleteFiles = new javax.swing.JCheckBox();
      jLabelAmountProcessed = new javax.swing.JLabel();
      menuBar = new javax.swing.JMenuBar();
      javax.swing.JMenu fileMenu = new javax.swing.JMenu();
      jMenuItemSaveConfiguration = new javax.swing.JMenuItem();
      jCheckBoxMenuItemStartMonitoring = new javax.swing.JCheckBoxMenuItem();
      jSeparator1 = new javax.swing.JPopupMenu.Separator();
      javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
      javax.swing.JMenu helpMenu = new javax.swing.JMenu();
      javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
      statusPanel = new javax.swing.JPanel();
      javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
      statusMessageLabel = new javax.swing.JLabel();
      statusAnimationLabel = new javax.swing.JLabel();
      progressBar = new javax.swing.JProgressBar();

      mainPanel.setName("mainPanel"); // NOI18N

      jPanel1.setName("jPanel1"); // NOI18N

      org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(emailmonitor.EmailMonitorApp.class).getContext().getResourceMap(EmailMonitorView.class);
      jTextFieldFromAddress.setText(resourceMap.getString("jTextFieldFromAddress.text")); // NOI18N
      jTextFieldFromAddress.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jTextFieldFromAddress.border.title"))); // NOI18N
      jTextFieldFromAddress.setName("jTextFieldFromAddress"); // NOI18N

      jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
      jLabel1.setName("jLabel1"); // NOI18N

      jScrollPane1.setName("jScrollPane1"); // NOI18N

      jListToAddresses.setModel(new DefaultListModel());
      jListToAddresses.setName("jListToAddresses"); // NOI18N
      jScrollPane1.setViewportView(jListToAddresses);

      jButtonAddEmailAddress.setText(resourceMap.getString("jButtonAddEmailAddress.text")); // NOI18N
      jButtonAddEmailAddress.setName("jButtonAddEmailAddress"); // NOI18N
      jButtonAddEmailAddress.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButtonAddEmailAddressActionPerformed(evt);
         }
      });

      jButtonRemoveEmailAddress.setText(resourceMap.getString("jButtonRemoveEmailAddress.text")); // NOI18N
      jButtonRemoveEmailAddress.setName("jButtonRemoveEmailAddress"); // NOI18N
      jButtonRemoveEmailAddress.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButtonRemoveEmailAddressActionPerformed(evt);
         }
      });

      jTextFieldEmailServerHost.setText(resourceMap.getString("jTextFieldEmailServerHost.text")); // NOI18N
      jTextFieldEmailServerHost.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jTextFieldEmailServerHost.border.title"))); // NOI18N
      jTextFieldEmailServerHost.setName("jTextFieldEmailServerHost"); // NOI18N

      jTextFieldEmailServerLoginName.setText(resourceMap.getString("jTextFieldEmailServerLoginName.text")); // NOI18N
      jTextFieldEmailServerLoginName.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jTextFieldEmailServerLoginName.border.title"))); // NOI18N
      jTextFieldEmailServerLoginName.setName("jTextFieldEmailServerLoginName"); // NOI18N

      jPasswordFieldMailServerLoginPassword.setText(resourceMap.getString("jPasswordFieldMailServerLoginPassword.text")); // NOI18N
      jPasswordFieldMailServerLoginPassword.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jPasswordFieldMailServerLoginPassword.border.title"))); // NOI18N
      jPasswordFieldMailServerLoginPassword.setName("jPasswordFieldMailServerLoginPassword"); // NOI18N

      jCheckBoxServerRequiresAuthentication.setText(resourceMap.getString("jCheckBoxServerRequiresAuthentication.text")); // NOI18N
      jCheckBoxServerRequiresAuthentication.setName("jCheckBoxServerRequiresAuthentication"); // NOI18N
      jCheckBoxServerRequiresAuthentication.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jCheckBoxServerRequiresAuthenticationActionPerformed(evt);
         }
      });

      jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
      jLabel2.setName("jLabel2"); // NOI18N

      jSpinnerSmtpPort.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(25), Integer.valueOf(1), null, Integer.valueOf(1)));
      jSpinnerSmtpPort.setName("jSpinnerSmtpPort"); // NOI18N

      jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
      jLabel3.setName("jLabel3"); // NOI18N

      jScrollPane2.setName("jScrollPane2"); // NOI18N

      jListMonitorFolders.setModel(new DefaultListModel());
      jListMonitorFolders.setName("jListMonitorFolders"); // NOI18N
      jScrollPane2.setViewportView(jListMonitorFolders);

      jButtonAddFolder.setText(resourceMap.getString("jButtonAddFolder.text")); // NOI18N
      jButtonAddFolder.setName("jButtonAddFolder"); // NOI18N
      jButtonAddFolder.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButtonAddFolderActionPerformed(evt);
         }
      });

      jButtonRemoveFolder.setText(resourceMap.getString("jButtonRemoveFolder.text")); // NOI18N
      jButtonRemoveFolder.setName("jButtonRemoveFolder"); // NOI18N
      jButtonRemoveFolder.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButtonRemoveFolderActionPerformed(evt);
         }
      });

      jCheckBoxDeleteFiles.setText(resourceMap.getString("jCheckBoxDeleteFiles.text")); // NOI18N
      jCheckBoxDeleteFiles.setName("jCheckBoxDeleteFiles"); // NOI18N

      jLabelAmountProcessed.setText(resourceMap.getString("jLabelAmountProcessed.text")); // NOI18N
      jLabelAmountProcessed.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
      jLabelAmountProcessed.setName("jLabelAmountProcessed"); // NOI18N

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(18, 18, 18)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addComponent(jLabelAmountProcessed, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(38, 38, 38)
                  .addComponent(jButtonAddFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(50, 50, 50)
                  .addComponent(jButtonRemoveFolder))
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addGap(97, 97, 97)
                  .addComponent(jButtonAddEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(93, 93, 93)
                  .addComponent(jButtonRemoveEmailAddress))
               .addComponent(jPasswordFieldMailServerLoginPassword)
               .addComponent(jTextFieldEmailServerLoginName)
               .addComponent(jTextFieldEmailServerHost, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addComponent(jCheckBoxServerRequiresAuthentication)
                  .addGap(6, 6, 6)
                  .addComponent(jCheckBoxDeleteFiles)
                  .addGap(18, 18, 18)
                  .addComponent(jLabel2)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jSpinnerSmtpPort, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jScrollPane1)
               .addComponent(jScrollPane2)
               .addComponent(jTextFieldFromAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 483, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(26, Short.MAX_VALUE))
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jTextFieldFromAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jLabel1)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jButtonAddEmailAddress)
               .addComponent(jButtonRemoveEmailAddress))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jTextFieldEmailServerHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addComponent(jTextFieldEmailServerLoginName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jPasswordFieldMailServerLoginPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jCheckBoxServerRequiresAuthentication)
               .addComponent(jSpinnerSmtpPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel2)
               .addComponent(jCheckBoxDeleteFiles))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel3)
            .addGap(3, 3, 3)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jLabelAmountProcessed)
               .addComponent(jButtonAddFolder)
               .addComponent(jButtonRemoveFolder))
            .addContainerGap(19, Short.MAX_VALUE))
      );

      javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
      mainPanel.setLayout(mainPanelLayout);
      mainPanelLayout.setHorizontalGroup(
         mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(mainPanelLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(22, Short.MAX_VALUE))
      );
      mainPanelLayout.setVerticalGroup(
         mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(mainPanelLayout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      menuBar.setName("menuBar"); // NOI18N

      fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
      fileMenu.setName("fileMenu"); // NOI18N

      jMenuItemSaveConfiguration.setText(resourceMap.getString("jMenuItemSaveConfiguration.text")); // NOI18N
      jMenuItemSaveConfiguration.setName("jMenuItemSaveConfiguration"); // NOI18N
      jMenuItemSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMenuItemSaveConfigurationActionPerformed(evt);
         }
      });
      fileMenu.add(jMenuItemSaveConfiguration);

      jCheckBoxMenuItemStartMonitoring.setText(resourceMap.getString("jCheckBoxMenuItemStartMonitoring.text")); // NOI18N
      jCheckBoxMenuItemStartMonitoring.setName("jCheckBoxMenuItemStartMonitoring"); // NOI18N
      jCheckBoxMenuItemStartMonitoring.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jCheckBoxMenuItemStartMonitoringActionPerformed(evt);
         }
      });
      fileMenu.add(jCheckBoxMenuItemStartMonitoring);

      jSeparator1.setName("jSeparator1"); // NOI18N
      fileMenu.add(jSeparator1);

      javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(emailmonitor.EmailMonitorApp.class).getContext().getActionMap(EmailMonitorView.class, this);
      exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
      exitMenuItem.setName("exitMenuItem"); // NOI18N
      fileMenu.add(exitMenuItem);

      menuBar.add(fileMenu);

      helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
      helpMenu.setName("helpMenu"); // NOI18N

      aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
      aboutMenuItem.setName("aboutMenuItem"); // NOI18N
      helpMenu.add(aboutMenuItem);

      menuBar.add(helpMenu);

      statusPanel.setName("statusPanel"); // NOI18N

      statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

      statusMessageLabel.setName("statusMessageLabel"); // NOI18N

      statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

      progressBar.setName("progressBar"); // NOI18N

      javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
      statusPanel.setLayout(statusPanelLayout);
      statusPanelLayout.setHorizontalGroup(
         statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
         .addGroup(statusPanelLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(statusMessageLabel)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 399, Short.MAX_VALUE)
            .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(statusAnimationLabel)
            .addContainerGap())
      );
      statusPanelLayout.setVerticalGroup(
         statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(statusPanelLayout.createSequentialGroup()
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(statusMessageLabel)
               .addComponent(statusAnimationLabel)
               .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(3, 3, 3))
      );

      setComponent(mainPanel);
      setMenuBar(menuBar);
      setStatusBar(statusPanel);
   }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddEmailAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddEmailAddressActionPerformed
       String email = (String) JOptionPane.showInputDialog("Enter email address:");
       if (null != email && !email.isEmpty())
       {
         emailCfg.toAddresses.add(email);
         ((DefaultListModel) jListToAddresses.getModel()).addElement(email);
       }
    }//GEN-LAST:event_jButtonAddEmailAddressActionPerformed

    private void jButtonRemoveEmailAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoveEmailAddressActionPerformed
        removeSelectedItems(jListToAddresses);
    }//GEN-LAST:event_jButtonRemoveEmailAddressActionPerformed

    private void jButtonAddFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddFolderActionPerformed
       JFileChooser fileChooser = new JFileChooser();
       fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

       int retVal = fileChooser.showOpenDialog(null);

       if (retVal == JFileChooser.APPROVE_OPTION)
       {
          String folderName = fileChooser.getSelectedFile().getPath();
          if (null != folderName && !folderName.isEmpty())
          {
            ((DefaultListModel) jListMonitorFolders.getModel()).addElement(folderName);
          }
       }
    }//GEN-LAST:event_jButtonAddFolderActionPerformed

    private void jButtonRemoveFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoveFolderActionPerformed

        try
        {
          if (!jListMonitorFolders.isSelectionEmpty())
          {
             Object[] rmList = jListMonitorFolders.getSelectedValues();
             for (int i=0; i<rmList.length; i++)
             {
                ((DefaultListModel) jListMonitorFolders.
                                getModel()).removeElement(rmList[i]);
             }
          }
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
    }//GEN-LAST:event_jButtonRemoveFolderActionPerformed

    private void jCheckBoxServerRequiresAuthenticationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxServerRequiresAuthenticationActionPerformed
        jTextFieldEmailServerLoginName.setEnabled(jCheckBoxServerRequiresAuthentication.isSelected());
        jPasswordFieldMailServerLoginPassword.setEnabled(jCheckBoxServerRequiresAuthentication.isSelected());        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxServerRequiresAuthenticationActionPerformed

    private void jMenuItemSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSaveConfigurationActionPerformed

        emailCfg.monitorFolders.clear();
        for (Object o : ((DefaultListModel) jListMonitorFolders.getModel()).toArray())
        {
            String str = (String)o;
            emailCfg.monitorFolders.add(str);
        }
        emailCfg.toAddresses.clear();
        for (Object o : ((DefaultListModel) jListToAddresses.getModel()).toArray())
        {
            String str = (String)o;
            emailCfg.toAddresses.add(str);
        }
        emailCfg.serverRequiresAuth = jCheckBoxServerRequiresAuthentication.isSelected();
        char[] pwd = jPasswordFieldMailServerLoginPassword.getPassword();
        emailCfg.loginPassword = String.copyValueOf(pwd);
        emailCfg.loginName = jTextFieldEmailServerLoginName.getText();
        emailCfg.serverPort = ((SpinnerNumberModel)jSpinnerSmtpPort.getModel())
                                                        .getNumber().intValue();
        emailCfg.serverHost = jTextFieldEmailServerHost.getText();
        emailCfg.fromAddress = jTextFieldFromAddress.getText();

        OutputStream os;
        try
        {
          os = new FileOutputStream(CONFIG_FILENAME);
          ObjectOutput oo = new ObjectOutputStream(os);
          oo.writeObject(emailCfg);
          oo.close();
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
    }//GEN-LAST:event_jMenuItemSaveConfigurationActionPerformed

    private void jCheckBoxMenuItemStartMonitoringActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItemStartMonitoringActionPerformed

       if (jCheckBoxMenuItemStartMonitoring.isSelected())
       {
          emailCfg.mailSession = EmailUtils.initializeEmail(emailCfg.serverHost,
                                                            emailCfg.serverPort,
                                                            emailCfg.fromAddress,
                                                            Utils.MAIL_CFG_FILE_NAME);
          updateTimer.start();
          busyIconTimer.start();
       }
       else
       {
          updateTimer.stop();
          busyIconTimer.stop();
          statusAnimationLabel.setIcon(idleIcon);
       }
    }//GEN-LAST:event_jCheckBoxMenuItemStartMonitoringActionPerformed

   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton jButtonAddEmailAddress;
   private javax.swing.JButton jButtonAddFolder;
   private javax.swing.JButton jButtonRemoveEmailAddress;
   private javax.swing.JButton jButtonRemoveFolder;
   private javax.swing.JCheckBox jCheckBoxDeleteFiles;
   private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItemStartMonitoring;
   private javax.swing.JCheckBox jCheckBoxServerRequiresAuthentication;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JLabel jLabel2;
   private javax.swing.JLabel jLabel3;
   private javax.swing.JLabel jLabelAmountProcessed;
   private javax.swing.JList jListMonitorFolders;
   private javax.swing.JList jListToAddresses;
   private javax.swing.JMenuItem jMenuItemSaveConfiguration;
   private javax.swing.JPanel jPanel1;
   private javax.swing.JPasswordField jPasswordFieldMailServerLoginPassword;
   private javax.swing.JScrollPane jScrollPane1;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JPopupMenu.Separator jSeparator1;
   private javax.swing.JSpinner jSpinnerSmtpPort;
   private javax.swing.JTextField jTextFieldEmailServerHost;
   private javax.swing.JTextField jTextFieldEmailServerLoginName;
   private javax.swing.JTextField jTextFieldFromAddress;
   private javax.swing.JPanel mainPanel;
   private javax.swing.JMenuBar menuBar;
   private javax.swing.JProgressBar progressBar;
   private javax.swing.JLabel statusAnimationLabel;
   private javax.swing.JLabel statusMessageLabel;
   private javax.swing.JPanel statusPanel;
   // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;

    public boolean removeSelectedItems(javax.swing.JList listBox)
    {
       boolean removed = true;
       if (!listBox.isSelectionEmpty())
       {
          for (Object o : listBox.getSelectedValues())
          {
            ((DefaultListModel)listBox.getModel()).removeElement(o);
          }
       }
       else
       {
          removed = false;
       }
       return removed;
    }

 /**
   * Timer to automatically repaint the event display and implements the
   * auto-scroll strip chart behavior
   */
  protected class MonitorFoldersActionListener
    implements ActionListener
  {
    @Override
    public void actionPerformed(ActionEvent e)
    {
      System.out.println("Processing folders.");
      for (String fldr : emailCfg.monitorFolders)
      {
        File dir = new File(fldr);
        String[] children = dir.list();

        for (String str : children)
        {
          System.out.println(str);
        }

        if (children == null)
        {
          System.out.println("No files to process!");
        }
        else
        {
          try
          {
            for (String str : children)
            {
              String childName = fldr + "/" + str;
              BufferedReader in = new BufferedReader(new FileReader(childName));
              StringBuilder sb = new StringBuilder();
              if (in.ready())
              {
                String line;
                while ((line = in.readLine()) != null)
                {
                   sb.append(line);
                   System.out.println(line);
                }
              }
              in.close();
              if (jCheckBoxDeleteFiles.isSelected())
              {
                 File fdel = new File(childName);
                 fdel.delete();
              }
              for (String toaddr : emailCfg.toAddresses)
              {
                EmailUtils.createEmailMessage(emailCfg, toaddr,
                 Utils.dtgDateFormat.format(new Date()), sb.toString(), true);
              }
              amountProcessed++;
            }
            jLabelAmountProcessed.setText("Processed: " + amountProcessed.toString());
          }
          catch (IOException ex)
          {
            ex.printStackTrace();
          }
        }
      }
    }
  }
}

